# People stats

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install helloworld.

Use node v14 use :
```bash
nvm install 16
```

```bash
npm i
```

## Usage

Start the application dev with :

```bash
npm run start
```

Created the dist with :

```bash
npm run dist
```

Analyse the coding rules with :

```bash
npm run lint
```

## Routes

Here are all the routes of the application : 

* `/login` : This is the login page
* `/` : This is the home page where you can find also the dashboard (You have to login first)
* `/search` : You can find all the users from the database and you can also filter to find specifics users
* `/search/:username` : You can see the information of one user

**If you use another route from this list, you will redirected to 404 page not found**
## Dependencies

To make the chart I used the highcharts-react-official library

To make the map, I used react-map-gl. Here is the official documentation : "https://visgl.github.io/react-map-gl/"

