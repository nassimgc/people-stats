import React, { useState, useEffect } from 'react';
import {
  Container, Navbar, Nav, Image
} from 'react-bootstrap';
import { useCookies } from 'react-cookie';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const CommonNavbar = () => {
  // We retrieves current user logged
  const user = useSelector((state) => state.auth.user);
  // We initialize the datetime which appear in navbar
  const [dateState, setDateState] = useState(new Date());
  // Create a dynamic clock
  useEffect(() => {
    const dateTime = setInterval(() => setDateState(new Date()), 1000);
    // Avoid errors with the time when changing pages
    return () => clearInterval(dateTime);
  }, []);
  const [, , removeCookie] = useCookies(['login', 'username']);

  // We initialize variable for the datetime
  const time = `${
    (dateState.getHours() < 10 ? '0' : '') + dateState.getHours()
  }:${(dateState.getMinutes() < 10 ? '0' : '') + dateState.getMinutes()}:${
    (dateState.getSeconds() < 10 ? '0' : '') + dateState.getSeconds()
  }`;
  function logout() {
    // Cookies are removed to establish the disconnection
    removeCookie('login');
    removeCookie('username');
  }
  return (
    <>
      <Navbar bg='primary' variant='dark' expand='lg'>
        <Container>
          <Navbar.Brand as={Link} to='/'>
            People-stats
          </Navbar.Brand>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='me-auto'>
              <Nav.Link as={Link} to='/'>
                Home
              </Nav.Link>
              <Nav.Link as={Link} to='/search'>
                Search
              </Nav.Link>
              <Nav.Link as={Link} to='/login' onClick={logout}>
                Logout
              </Nav.Link>
            </Nav>
            <Nav className='ms-auto'>
              <Navbar.Text>
                {user.name.title} {user.name.first} {user.name.last} {time}
                <Image src={user.picture.thumbnail} rounded />
              </Navbar.Text>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
};

export default CommonNavbar;
