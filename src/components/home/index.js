import React from 'react';
import {
  Card, Col, Container, Row
} from 'react-bootstrap';
import { useSelector } from 'react-redux';
import HomeMap from '../maps/home-map';
import Gender from '../stats/gender-chart';
import Country from '../stats/country-chart';
import RankCountriesUsers from '../stats/rank-countries-users';

const Accueil = () => {
  // We recovers the first 100 users from database
  const usersMaps = useSelector((state) => state.stats.usersMaps);
  // We recovers the number of users from database
  const usersNumber = useSelector((state) => state.stats.userNumber);
  return (
    <Container className="mt-3" fluid>
      <Row className="mt-3">
        <Col md={12}>
          <Card>
            <Card.Header className="text-center">Number of registered users</Card.Header>
            <Card.Body className='text-center'>
              {usersNumber}
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <Row className="mt-3">
        <Col md={6}>
          <Card>
            <Card.Header className="text-center">Gender</Card.Header>
            <Card.Body>
              <Gender/>
            </Card.Body>
          </Card>
        </Col>
        <Col md={6}>
          <Card>
            <Card.Header className="text-center">Country</Card.Header>
            <Card.Body>
              <Country/>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <Row className="mt-3">
        <Col md={12}>
          <Card>
            <Card.Header className="text-center">Number of people from the first 15 countries with the most people.</Card.Header>
            <Card.Body className='text-center'>
              <RankCountriesUsers/>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <Row className="mt-3">
        <Col md={12}>
          <Card>
            <Card.Header className="text-center">Maps</Card.Header>
            <Card.Body>
              <HomeMap users={usersMaps} />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default Accueil;
