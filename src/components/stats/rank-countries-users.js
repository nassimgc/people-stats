import React from 'react';
import { Image, Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';

const RankCountriesUsers = () => {
  const userByCountryNbRank = useSelector(
    (state) => state.stats.userByCountryNbRank
  );
  return (
    <Table striped bordered hover responsive>
      <tbody>
        {Object.keys(userByCountryNbRank).map((key) => (
          <tr key={key}>
            <th scope='row'>
              <Image
                width='25'
                src={`https://countryflagsapi.com/png/${key}`}
              />
            </th>
            <td>{userByCountryNbRank[key]}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default RankCountriesUsers;
