import React from 'react';
import { Button, Form } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';

const SearchFilter = () => {
  // We init the react-hook-form for the filter
  const { register, handleSubmit } = useForm();
  // We call a dispatch to send new users object if filter is submit
  const dispatch = useDispatch();
  // We recovers all users from database
  const allUsers = useSelector((state) => state.search.users);

  function onSubmit(data) {
    let users = allUsers;
    // We update the filter data
    dispatch({
      type: 'data/update',
      filter: data
    });

    /* By default filter value are :
    gender: 'Genre',
    city: '', state: '',
    nameFirst: '',
    nameLast: '' */

    // We check the gender input
    if (data.gender !== 'Genre') {
      users = users.filter((v) => v.gender === data.gender);
    }

    // We check the city input
    if (data.city !== '') {
      const { city } = data;
      users = users.filter(
        (v) => v.location.city === city.charAt(0).toUpperCase() + city.toLowerCase().slice(1)
      );
    }

    // We check the state input
    if (data.state !== '') {
      const { state } = data;
      users = users.filter(
        (v) => v.location.state === state.charAt(0).toUpperCase() + state.toLowerCase().slice(1)
      );
    }

    // We check the first name input
    if (data.nameFirst !== '') {
      const { nameFirst } = data;
      users = users.filter(
        (v) => v.name.first === nameFirst.charAt(0).toUpperCase() + nameFirst.toLowerCase().slice(1)
      );
    }

    // We check the last name input
    if (data.nameLast !== '') {
      const { nameLast } = data;
      users = users.filter(
        (v) => v.name.last === nameLast.charAt(0).toUpperCase() + nameLast.toLowerCase().slice(1)
      );
    }
    // Reset the current page
    dispatch({
      type: 'initialPage/update',
      initialPage: 0
    });
    // The minimum data scale is reset
    dispatch({
      type: 'itemOffSet/update',
      itemOffSet: 0
    });
    // We send the users based on filter if the form is submit
    dispatch({
      type: 'usersFilter/update',
      usersFilter: users
    });
  }
  // We return here the form that we will call inside search.js
  return (<div className='d-flex justify-content-center'>
            <Form className='d-flex mt-5' onSubmit={handleSubmit(onSubmit)}>
                <Form.Select {...register('gender')} aria-label='Default select example'>
                    <option value='Genre'>Gender</option>
                    <option value='male'>Male</option>
                    <option value='female'>Female</option>
                </Form.Select>
                <Form.Control type='text' placeholder='City' {...register('city')}/>
                <Form.Control type='text' placeholder='State' {...register('state')} />
                <Form.Control type='text' placeholder='first name' {...register('nameFirst')} />
                <Form.Control type='text' placeholder='last name' {...register('nameLast')}/>
                <Button variant='primary' type='submit'>
                    Submit
                </Button>
            </Form>
        </div>);
};

export default SearchFilter;
