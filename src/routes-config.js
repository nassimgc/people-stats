import React from 'react';
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate,
  useParams
} from 'react-router-dom';
import { useCookies } from 'react-cookie';
import { useDispatch } from 'react-redux';
import _ from 'lodash';
import db from '../dbusers.json';
import Accueil from './components/home';
import Login from './components/login';
import Search from './components/search/search';
import User from './components/search/user';
import CommonNavbar from './components/common/navbar';
import Error from './components/common/error';

// Private element when the user is logged
const Private = ({ Component }) => {
  const [cookie] = useCookies(['login', 'username']);
  const dispatch = useDispatch();

  if (cookie.login !== 'true') {
    return <Navigate to='/login' />;
  }
  dispatch({
    type: 'isConnected/update',
    isConnected: true
  });
  const user = db.results.filter((v) => v.login.username === cookie.username);
  dispatch({
    type: 'userConnected/update',
    user: user[0]
  });

  dispatch({
    type: 'userNumber/update',
    userNumber: db.results.length
  });

  const countGenre = { male: 0, female: 0 };

  db.results.forEach((item) => {
    countGenre[item.gender] += 1;
  });
  dispatch({
    type: 'userMan/update',
    userManNumber: countGenre.male
  });

  dispatch({
    type: 'userWoman/update',
    userWomanNumber: countGenre.female
  });
  // We create an object to get number of user by each country
  let userNbByCountry = _.countBy(db.results, 'location.country');
  // We create from the first object an array of object to make it sortable easily
  userNbByCountry = Object.keys(userNbByCountry).map((key) => (
    { countryLabel: key, count: userNbByCountry[key] }));
  // We sort users number by country
  userNbByCountry = _.sortBy(userNbByCountry, 'count').reverse();
  // We init object with number of user for all countries
  const allUserNbByCountry = {};
  // We init object with number of user for the best 15 countries populated
  const BestUserNbByCountry = {};

  // For each element of the array object we create an entry (key : value) in the finals object
  userNbByCountry.forEach((country) => {
    allUserNbByCountry[country.countryLabel] = country.count;
  });
  userNbByCountry.slice(0, 15).forEach((country) => {
    BestUserNbByCountry[country.countryLabel] = country.count;
  });

  dispatch({
    type: 'userByCountry/update',
    userByCountryNumber: allUserNbByCountry
  });

  dispatch({
    type: 'userByCountryRank/update',
    userByCountryNbRank: BestUserNbByCountry
  });

  dispatch({
    type: 'usersMaps/update',
    usersMaps: db.results.slice(0, 100)
  });

  return (
    <>
      <CommonNavbar />
      <Component />
    </>
  );
};

// Public element when the user is not logged
const Public = ({ Component }) => {
  const [cookie] = useCookies(['login']);
  if (cookie.login === 'true') {
    return <Navigate to='/' />;
  }

  return <Component />;
};

// Private element when the user is logged and want to search some users
const PrivateSearch = ({ Component }) => {
  const [cookie] = useCookies(['login', 'username']);
  const dispatch = useDispatch();

  if (cookie.login !== 'true') {
    return <Navigate to='/login' />;
  }
  dispatch({
    type: 'isConnected/update',
    isConnected: true
  });
  const user = db.results.filter((v) => v.login.username === cookie.username);
  dispatch({
    type: 'userConnected/update',
    user: user[0]
  });

  dispatch({
    type: 'users/update',
    users: db.results
  });
  return (
    <>
      <CommonNavbar />
      <Component />
    </>
  );
};
// Private element when the user is logged and want to see a user selected in the search page result
const PrivateUser = ({ Component }) => {
  const [cookie] = useCookies(['login', 'username']);
  const dispatch = useDispatch();
  const { username } = useParams();

  // We check if the cookie 'login' is set
  if (cookie.login !== 'true') {
    // If is not set then user return to the login page
    return <Navigate to='/login' />;
  }
  dispatch({
    type: 'isConnected/update',
    isConnected: true
  });
  const user = db.results.filter((v) => v.login.username === cookie.username);
  dispatch({
    type: 'userConnected/update',
    user: user[0]
  });

  const userResult = db.results.filter((v) => v.login.username === username);
  dispatch({
    type: 'userResult/update',
    userResult: userResult[0]
  });

  return (
    <>
      <CommonNavbar />
      <Component />
    </>
  );
};
// Here we declare our routes
const RoutesConfig = () => (
  <BrowserRouter>
    <Routes>
      <Route path='/' element={<Private Component={Accueil} />} />
      <Route path='/login' element={<Public Component={Login} />} />
      <Route path='/search' element={<PrivateSearch Component={Search} />} />
      <Route
        path='/user/:username'
        element={<PrivateUser Component={User} />}
      />
      <Route path='*' element={<Error />} />
    </Routes>
  </BrowserRouter>
);

export default RoutesConfig;
